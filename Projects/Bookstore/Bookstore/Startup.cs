using Bookstore.DbContexts;
using Bookstore.Modules.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bookstore
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BookstoreContext>(options =>
                           options.UseSqlServer(configuration["Data:ConnectionString"]));

            services.AddMvc();

            RegisterDependensies(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc(routeBuilder =>
            {
                routeBuilder.MapRoute(
                     name: "default",
                     template: "{controller=Home}/{action=Index}");
            });
        }

        private void RegisterDependensies(IServiceCollection services)
        {
            services.RegisterRepositoriesModule();
        }
    }
}