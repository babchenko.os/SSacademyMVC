﻿namespace Bookstore.Entities
{
    public class BookCategoryBook
    {
        public long BookCategoryId { get; set; }
        public long BookId { get; set; }
        public virtual BookCategory BookCategory { get; set; }
        public virtual Book Book { get; set; }
    }
}