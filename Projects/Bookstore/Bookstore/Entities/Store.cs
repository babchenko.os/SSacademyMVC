﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bookstore.Entities
{
    public class Store
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        public virtual Address Address { get; set; }
        public virtual IEnumerable<StoreBook> Books { get; set; }
    }
}