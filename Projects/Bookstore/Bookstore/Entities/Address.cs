﻿using System.ComponentModel.DataAnnotations;

namespace Bookstore.Entities
{
    public class Address
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public int House { get; set; }
        public int Flat { get; set; }
    }
}