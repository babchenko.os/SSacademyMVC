﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bookstore.Entities
{
    public class BookCategory
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual IEnumerable<BookCategory> Books { get; set; }
    }
}