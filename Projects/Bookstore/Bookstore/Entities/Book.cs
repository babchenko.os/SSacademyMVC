﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bookstore.Entities
{
    public class Book
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string BriefSynopsis { get; set; }
        public DateTime PublishDate { get; set; }

        public virtual ICollection<BookCategoryBook> Categories { get; set; }
        public virtual IEnumerable<Author> Authors { get; set; }
    }
}