﻿using System.ComponentModel.DataAnnotations;

namespace Bookstore.Entities
{
    public class StoreBook
    {
        [Key]
        public long Id { get; set; }
        public virtual Store Store { get; set; }
        public virtual Book Book { get; set; }
    }
}