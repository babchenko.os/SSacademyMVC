﻿using System.Threading.Tasks;
using Bookstore.DbContexts;
using Bookstore.Entities;
using Bookstore.Repositories.Authors.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Bookstore.Repositories.Authors
{
    internal class AuthorsWriteRepository : IAuthorsWriteRepository
    {
        private readonly BookstoreContext dbContext;
        private readonly DbSet<Author> authors;

        public AuthorsWriteRepository(BookstoreContext dbContext)
        {
            this.dbContext = dbContext;
            this.authors = dbContext.Authors;
        }

        public Task AddNewAuthor(Author author)
        {
            authors.Add(author);
            return dbContext.SaveChangesAsync();
        }
    }
}