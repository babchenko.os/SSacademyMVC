﻿using Bookstore.Entities;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Authors.Interfaces
{
    public interface IAuthorsWriteRepository
    {
        Task AddNewAuthor(Author author);
    }
}