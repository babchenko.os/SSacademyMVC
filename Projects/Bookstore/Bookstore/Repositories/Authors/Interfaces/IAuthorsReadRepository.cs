﻿using Bookstore.Entities;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Authors.Interfaces
{
   public interface IAuthorsReadRepository
    {
        Task<Author[]> GetAllAuthors();
    }
}