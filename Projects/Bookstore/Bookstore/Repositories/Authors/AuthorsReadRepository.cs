﻿using Bookstore.DbContexts;
using Bookstore.Entities;
using Bookstore.Repositories.Authors.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Authors
{
    internal class AuthorsReadRepository : IAuthorsReadRepository
    {
        private readonly BookstoreContext dbContext;
        private readonly DbSet<Author> authors;

        public AuthorsReadRepository(BookstoreContext dbContext)
        {
            this.dbContext = dbContext;
            this.authors = dbContext.Authors;
        }
        public Task<Author[]> GetAllAuthors()
        {
            return authors.ToArrayAsync();
        }
    }
}
