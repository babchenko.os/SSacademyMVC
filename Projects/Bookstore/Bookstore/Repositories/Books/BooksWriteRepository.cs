﻿using Bookstore.DbContexts;
using Bookstore.Entities;
using Bookstore.Repositories.Books.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Bookstore.Repositories.Books
{
    public class BooksWriteRepository : IBooksWriteRepository
    {
        private readonly BookstoreContext dbContext;
        private readonly DbSet<Book> books;

        public BooksWriteRepository(BookstoreContext dbContext)
        {
            this.dbContext = dbContext;
            this.books = dbContext.Books;
        }

        public Task AddBookAsync(Book book)
        {
            books.Add(book);
            return dbContext.SaveChangesAsync();
        }
    }
}