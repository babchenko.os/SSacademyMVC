﻿using Bookstore.Entities;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Books.Interfaces
{
    public interface IBooksWriteRepository
    {
        Task AddBookAsync(Book book);
    }
}