﻿using Bookstore.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Books.Interfaces
{
    public interface IBooksReadRepository
    {
        Task<Book[]> GetBooksAsync(int page, int itemsPerPage);
    }
}