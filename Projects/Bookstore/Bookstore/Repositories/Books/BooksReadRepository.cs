﻿using System.Threading.Tasks;
using Bookstore.DbContexts;
using Bookstore.Entities;
using Bookstore.Repositories.Books.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Bookstore.Repositories.Books
{
    public class BooksReadRepository : IBooksReadRepository
    {
        private readonly BookstoreContext dbContext;
        private readonly DbSet<Book> books;

        public BooksReadRepository(BookstoreContext dbContext)
        {
            this.dbContext = dbContext;
            this.books = dbContext.Books;
        }

        public Task<Book[]> GetBooksAsync(int page, int itemsPerPage)
        {
            return books
                      .Skip(page * itemsPerPage)
                      .Take(itemsPerPage)
                      .ToArrayAsync();
        }
    }
}