﻿using Bookstore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Stores.Interfaces
{
    public interface IStoresWriteRepository
    {
        Task AddNewStoreAsync(Store store);
        Task RemoveStoreByIdAsync(long id);
    }
}
