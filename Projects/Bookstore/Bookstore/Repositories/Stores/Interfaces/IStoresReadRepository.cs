﻿using Bookstore.Entities;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Stores.Interfaces
{
    public interface IStoresReadRepository
    {
        Task<Store[]> GetTopRatedStoresAsync(int amount);
        Task<Store[]> GetStoresAsync(int page, int itemsPerPage);
        Task<int> GetStoresCount();
    }
}