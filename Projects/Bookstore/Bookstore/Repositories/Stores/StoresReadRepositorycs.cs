﻿using Bookstore.DbContexts;
using Bookstore.Entities;
using Bookstore.Repositories.Stores.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Stores
{
    internal class StoresReadRepositorycs : IStoresReadRepository
    {
        private readonly BookstoreContext dbContext;
        private readonly DbSet<Store> stores;

        public StoresReadRepositorycs(BookstoreContext dbContext)
        {
            this.dbContext = dbContext;
            this.stores = dbContext.Stores;
        }

        public Task<Store[]> GetTopRatedStoresAsync(int amount)
        {
            return stores
                   .GroupBy(store => store.Books.Count())
                   .SelectMany(stores => stores)
                   .Take(amount)
                   .ToArrayAsync();
        }

        public Task<Store[]> GetStoresAsync(int page, int itemsPerPage)
        {
            return stores
                      .Skip(page * itemsPerPage)
                      .Take(itemsPerPage)
                      .ToArrayAsync();
        }

        public Task<int> GetStoresCount()
        {
            return stores
                .CountAsync();
        }
    }
}