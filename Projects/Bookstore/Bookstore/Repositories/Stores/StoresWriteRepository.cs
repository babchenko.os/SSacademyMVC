﻿using Bookstore.DbContexts;
using Bookstore.Entities;
using Bookstore.Repositories.Stores.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bookstore.Repositories.Stores
{
    internal class StoresWriteRepository : IStoresWriteRepository
    {
        private readonly BookstoreContext dbContext;
        private readonly DbSet<Store> stores;

        public StoresWriteRepository(BookstoreContext dbContext)
        {
            this.dbContext = dbContext;
            this.stores = dbContext.Stores;
        }

        public Task AddNewStoreAsync(Store store)
        {
            stores.Add(store);
            return dbContext.SaveChangesAsync();
        }

        public Task RemoveStoreByIdAsync(long id)
        {
            var store = new Store { Id = id };
            stores.Remove(store);
            return dbContext.SaveChangesAsync();
        }
    }
}