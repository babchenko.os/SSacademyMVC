﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Bookstore.Migrations
{
    public partial class updatedbookstore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StoreBooks_Books_BookId",
                table: "StoreBooks");

            migrationBuilder.DropForeignKey(
                name: "FK_StoreBooks_Stores_StoreId",
                table: "StoreBooks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StoreBooks",
                table: "StoreBooks");

            migrationBuilder.AlterColumn<long>(
                name: "StoreId",
                table: "StoreBooks",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "BookId",
                table: "StoreBooks",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "StoreBooks",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_StoreBooks",
                table: "StoreBooks",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_StoreBooks_BookId",
                table: "StoreBooks",
                column: "BookId");

            migrationBuilder.AddForeignKey(
                name: "FK_StoreBooks_Books_BookId",
                table: "StoreBooks",
                column: "BookId",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StoreBooks_Stores_StoreId",
                table: "StoreBooks",
                column: "StoreId",
                principalTable: "Stores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StoreBooks_Books_BookId",
                table: "StoreBooks");

            migrationBuilder.DropForeignKey(
                name: "FK_StoreBooks_Stores_StoreId",
                table: "StoreBooks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StoreBooks",
                table: "StoreBooks");

            migrationBuilder.DropIndex(
                name: "IX_StoreBooks_BookId",
                table: "StoreBooks");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "StoreBooks");

            migrationBuilder.AlterColumn<long>(
                name: "StoreId",
                table: "StoreBooks",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BookId",
                table: "StoreBooks",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_StoreBooks",
                table: "StoreBooks",
                columns: new[] { "BookId", "StoreId" });

            migrationBuilder.AddForeignKey(
                name: "FK_StoreBooks_Books_BookId",
                table: "StoreBooks",
                column: "BookId",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StoreBooks_Stores_StoreId",
                table: "StoreBooks",
                column: "StoreId",
                principalTable: "Stores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
