﻿using Bookstore.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bookstore.DbContexts
{
    public class BookstoreContext : DbContext
    {
        public BookstoreContext(DbContextOptions<BookstoreContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<AuthorBook>()
                .HasKey(authorBook => new { authorBook.AuthorId, authorBook.BookId });

            modelBuilder
                .Entity<BookCategoryBook>()
                .HasKey(bookCategoryBook => new { bookCategoryBook.BookCategoryId, bookCategoryBook.BookId });

        }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<AuthorBook> AuthorBook { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookCategory> BookCategories { get; set; }
        public DbSet<BookCategoryBook> BookCategorieBook { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<StoreBook> StoreBooks { get; set; }
    }
}