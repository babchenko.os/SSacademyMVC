﻿using Bookstore.Repositories.Authors;
using Bookstore.Repositories.Authors.Interfaces;
using Bookstore.Repositories.Books;
using Bookstore.Repositories.Books.Interfaces;
using Bookstore.Repositories.Stores;
using Bookstore.Repositories.Stores.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Bookstore.Modules.Database
{
    public static class RepositoriesModule
    {
        public static IServiceCollection RegisterRepositoriesModule(this IServiceCollection services)
        {
            services.AddTransient<IBooksWriteRepository, BooksWriteRepository>();
            services.AddTransient<IBooksReadRepository, BooksReadRepository>();
            services.AddTransient<IStoresReadRepository, StoresReadRepositorycs>();
            services.AddTransient<IStoresWriteRepository, StoresWriteRepository>();
            services.AddTransient<IAuthorsReadRepository, AuthorsReadRepository>();
            services.AddTransient<IAuthorsWriteRepository, AuthorsWriteRepository>();
            
            return services;
        }
    }
}