﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bookstore.Models
{
    public class AuthorModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
    }
}