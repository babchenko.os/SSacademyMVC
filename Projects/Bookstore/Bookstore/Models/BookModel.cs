﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bookstore.Models
{
    public class BookModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime PublishDate { get; set; }
        [Required]
        public IEnumerable<long> AuthorsId { get; set; }
    }
}