﻿using System.ComponentModel.DataAnnotations;

namespace Bookstore.DTO
{
    public class StoreModel
    {
        [Required]
        [MinLength(3)]
        public string Name { get; set; }
    }
}