﻿using Bookstore.Entities;
using Bookstore.Models;
using Bookstore.Repositories.Authors.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Bookstore.Controllers
{
    public class AuthorsController : Controller
    {
        private readonly IAuthorsReadRepository authorsReadRepository;
        private readonly IAuthorsWriteRepository authorsWriteRepository;

        public AuthorsController(IAuthorsReadRepository authorsReadRepository,
            IAuthorsWriteRepository authorsWriteRepository)
        {
            this.authorsReadRepository = authorsReadRepository;
            this.authorsWriteRepository = authorsWriteRepository;
        }

        public async Task<IActionResult> Index()
        {
            var allAuthors = await authorsReadRepository.GetAllAuthors();

            return View(allAuthors);
        }

        public Task AddNewAuthor(AuthorModel authorDTO)
        {
            var author = new Author
            {
                Name = authorDTO.Name,
                DateOfBirth = authorDTO.DateOfBirth
            };

            return authorsWriteRepository.AddNewAuthor(author);
        }
    }
}
