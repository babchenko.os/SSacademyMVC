﻿using Bookstore.DTO;
using Bookstore.Entities;
using Bookstore.Repositories.Stores.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Bookstore.Controllers
{
    public class HomeController : Controller
    {
        private const int itemsPerPage = 5;

        private readonly IStoresReadRepository storesReadRepository;
        private readonly IStoresWriteRepository storesWriteRepository;

        public HomeController(IStoresReadRepository storesReadRepository,
            IStoresWriteRepository storesWriteRepository)
        {
            this.storesReadRepository = storesReadRepository;
            this.storesWriteRepository = storesWriteRepository;
        }

        public async Task<IActionResult> Index(int? storePage)
        {
            var currentPage = storePage ?? 0;

            ViewBag.topRatedStores = await storesReadRepository.GetTopRatedStoresAsync(5);
            ViewBag.displayedStores = await storesReadRepository.GetStoresAsync(currentPage, itemsPerPage);

            var totalStoresCount = await storesReadRepository.GetStoresCount();

            var pagesCount = totalStoresCount / itemsPerPage;

            if (totalStoresCount % itemsPerPage > 0)
                pagesCount++;

            ViewBag.currentPage = currentPage;
            ViewBag.pagesCount = pagesCount;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddNewStore(StoreModel storeDTO)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Index");

            var store = new Store
            {
                Name = storeDTO.Name
            };

            await storesWriteRepository.AddNewStoreAsync(store);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> RemoveStore(int? storeId)
        {
            if (storeId == null)
                return RedirectToAction("Index");

            await storesWriteRepository.RemoveStoreByIdAsync(storeId.Value);

            return RedirectToAction("Index");
        }
    }
}