﻿using Bookstore.Entities;
using Bookstore.Repositories.Books.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Bookstore.Controllers
{
    public class BooksController : Controller
    {
        private readonly IBooksWriteRepository booksWriteRepository;
        private readonly IBooksReadRepository booksReadRepository;

        public BooksController(IBooksWriteRepository booksWriteRepository,
            IBooksReadRepository booksReadRepository)
        {
            this.booksWriteRepository = booksWriteRepository;
            this.booksReadRepository = booksReadRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public Task AddNewBook(Book newBook)
        {
            return booksWriteRepository.AddBookAsync(newBook);
        }
    }
}